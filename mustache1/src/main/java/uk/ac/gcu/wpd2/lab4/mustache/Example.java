package uk.ac.gcu.wpd2.lab4.mustache;

import java.util.Arrays;
import java.util.List;

public class Example {

    List<Item> items() {
        return Arrays.asList(
                new Item("Item 1", "$19.99", Arrays.asList(new Feature("New!"),
                        new Feature("Awesome!"))),
                new Item("Item 2", "$29.99", Arrays.asList(new Feature("Old."),
                        new Feature("Ugly.")))
        );
    }

    static class Item {
        String name, price;
        List<Feature> features;

        Item(String name, String price, List<Feature> features) {
            this.name = name;
            this.price = price;
            this.features = features;
        }
    }

    static class Feature {
        Feature(String description) {
            this.description = description;
        }
        String description;
    }
}


