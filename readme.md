# Title
Maven command line example using Mustache for Java

# Motivation
Example of rendering output from a domain object and mustache template

# Technologies used
- IntelliJ IDE
- Maven
- Jdk 11
- Mustache Compiler 0.9.6

# Installation
Import into IntelliJ as a Maven project. Run Main.main(). No command line arguments.

# License
For use in WPD2.